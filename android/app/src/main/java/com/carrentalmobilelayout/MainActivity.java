package com.carrentalmobilelayout;
import android.os.Bundle;
import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {
  @Override
  protected String getMainComponentName() {
    return "CarRentalMobileLayout";
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(null);
}
}
