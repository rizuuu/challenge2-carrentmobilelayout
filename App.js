import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from './src/screens/HomeScreen';
import AccountScreen from './src/screens/AccountScreen';
import CarListScreen from './src/screens/CarListScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import react from 'react';


const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

// screen name 
const homeName = 'Home';
const carListName = 'Daftar Mobil';
const akunName = 'Akun';

function App() {  
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Tab1} options={{ headerShown: false }} />
        <Stack.Screen name="Daftar Mobil" component={CarListScreen} />
        <Stack.Screen name="Akun" component={AccountScreen} />        
      </Stack.Navigator>      
    </NavigationContainer>
  );
}

export default App;

export function Tab1(){  
  return(
    <Tab.Navigator
      initialRouteName={homeName}
      screenOptions = {({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          let rn = route.name;

          if (rn === homeName){
            iconName = focused ? 'home' : 'home-outline';
          } else if (rn === carListName){
            iconName = focused ? 'list' : 'list-outline';
          } else if (rn === akunName) {
            iconName = focused ? 'person' : 'person-outline';
          }

          return <Ionicons name={iconName} size={size} color={color}/>;

        },                
      })}      
      >
      
        <Tab.Screen name={homeName} component={HomeScreen} options={{ headerShown: false }}/>
        <Tab.Screen name={carListName} component={CarListScreen} />
        <Tab.Screen name={akunName} component={AccountScreen}/>
    </Tab.Navigator>
  )
}