import * as React from 'react';
import { Text, View, StyleSheet, Image, ScrollView} from 'react-native';
import IconMenu from '../components/IconMenu';
import CarListComponnent from '../components/CarListComponnent';

export default function HomeScreen(){
    return(
        <View style={{ flex: 1 }}>
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.top}>
                        <View style={styles.subTop}>
                            <View>
                                <Text style={{fontSize: 12}}>Hi, Fariz</Text>
                                <Text style={{fontWeight: 'bold'}}>Cirebon, West Java</Text>
                            </View>
                            <Image source={require('../images/profile.png')}/>
                        </View>
                        <View style={styles.bannerWrapper}>
                        <Image
                            style={styles.banner}
                            source={require('../images/banner.jpg')}
                        />
                        </View>        
                    </View> 
                    <View style={styles.bottomRest}>
                        <View style={{alignItems: 'center'}}>
                            <View style={styles.menuWrapper}>
                                <IconMenu 
                                    title = "Sewa Mobil"
                                    url = {require('../images/fi_truck.png')}
                                />
                                <IconMenu 
                                    title = "Oleh - oleh"
                                    url = {require('../images/fi_box.png')}
                                />
                                <IconMenu 
                                    title = "Penginapan"
                                    url = {require('../images/fi_key.png')}
                                />
                                <IconMenu 
                                    title = "Wisata"
                                    url = {require('../images/fi_camera.png')}
                                />
                            </View>
                        </View>
                        <Text style={styles.scroller}>Daftar Mobil Pilihan</Text>
                        <CarListComponnent/>
                        <CarListComponnent/>
                        <CarListComponnent/>
                        <CarListComponnent/>                        
                    </View>                   
                </View>             
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    top: {
        backgroundColor: '#D3D9FD', 
        height: 150,       
        position: 'relative',
    },
    subTop: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        alignItems: 'center',
        marginTop: 20,
    },
    bannerWrapper: {
        width: '90%',
        height: 120,
        position: 'absolute',
        left: 20,
        bottom: -60,
        marginTop: 50          
    },
    banner: {       
        width: undefined,
        height: undefined,   
        flex: 1,
        resizeMode: 'cover',
        borderRadius: 10,
    },   
    bottomRest: {
        backgroundColor: 'white',
        marginTop: 80,
        marginBottom: 80,
    },           
    menuWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '90%',
    },    
    scroller: {
        fontWeight: 'bold',
        marginTop: 20,
        paddingHorizontal: 15,
    },    
})