import * as React from 'react';
import { Text, View, ScrollView } from 'react-native';
import CarListComponnent from '../components/CarListComponnent';

export default function CarListScreen(){
    return(
        <View style={{ backgroundColor: '#FFFFFF'}}>
            <ScrollView style={{ marginTop: 10 }}>
                <CarListComponnent/>
                <CarListComponnent/>
                <CarListComponnent/>
                <CarListComponnent/>
                <CarListComponnent/>
                <CarListComponnent/>
                <CarListComponnent/>
                <CarListComponnent/>
            </ScrollView>            
        </View>
    );
}