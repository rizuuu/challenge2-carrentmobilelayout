import * as React from 'react';
import { Text, View, Image, StyleSheet, Button } from 'react-native';

export default function AccountScreen(){
    return(
        <View style={styles.viewIndex}>
            <View>
                <Image style={styles.Image} source={require('../images/akunbg.png')}/>
            </View>
            <View>
                <Text style={styles.text}>
                    Upps kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR lebih mudah 
                </Text>
            </View>
            <View style={styles.topSize}>                
                <Button title='Register' color="#5CB85F" style={styles.btnRegister} />
            </View>
        </View>        
    );
}

const styles = StyleSheet.create({
    viewIndex: {
        backgroundColor: '#FFFFFF',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -20
    },
    backgroundImage: {        
        width: 312,
        height: 192,
        left: 0,
        top: 0,          
    },
    text:{
        fontFamily: 'Helvetica',
        fontSize: 14,
        alignItems: 'center',
    },    
    topSize:{
        paddingTop: 20,
    }
})